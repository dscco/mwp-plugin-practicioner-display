<?php 
/*
Plugin Name: Madewow's Practicioner Display
Plugin URI: https://madewow.com
Description: 
Version: 0.1
Author: MADEWOW
Author URI: http://www.madewow.com
License: GPL2
*/
global $pract_id;
$pract_id = $_GET['id'];
// require_once(plugin_dir_path(__FILE__) . 'practicioner-testimonial.php');

add_shortcode( 'madewow_practicioner_display', 'madewow_practicioner_display' );
function madewow_practicioner_display( $atts ){
	$pract_id = $_GET['id'];

	$search_criteria['field_filters'][] = array( 'key' => '16', 'value' => $pract_id );
	$search_criteria['status'] = 'active';
	$entry = GFAPI::get_entries(1,$search_criteria);
	$entry = $entry[0];

	if ($entry) {
		$content = '';
		$content .='<h2>'.$entry['1.3'].' '.$entry['1.6'].'</h2>';

		$content .='<br>';
		
		$content .= '<h3>Practicioner Type</h3>';
		$content .= '<p>'.$entry['20'].'</p>';

		// $content .='<br>';

		$content .= '<h3>Contitions</h3>';
		$formdata = GFAPI::get_form(1);
		$checkboxdata='';

		foreach($formdata['fields'] as $key => $value){
			if($value['type'] == 'checkbox'){
				for ($i=1; $i <= count($value['choices']) ; $i++) { 
					if ($entry[$value['id'].'.'.$i]) {
						$checkboxdata .= $entry[$value['id'].'.'.$i];
						if ($i != count($value['choices'])) {
							$checkboxdata .= ',';
						}
					}
				}
			}
		}
		$content .= '<p>'.rtrim($checkboxdata,',').'</p>';

		// $content .='<br>';

		$content .= '<h3>Language</h3>';
		$content .= '<p>'.$entry['4'].'</p>';

		$content .= '<h3>Address</h3>';
		$content .= '<p>'.$entry['5.1'].'';
		$content .= '<br>'.$entry['5.2'].'';
		$content .= '<br>'.$entry['5.3'].', '.$entry['5.4'];
		$content .= '<br>'.$entry['5.5'].', '.$entry['5.6'].'</p>';

		$content .= '<h3>Phone</h3>';
		$content .= '<p>'.$entry['6'].'</p>';
		$content .='<div id="map"></div>
					<style>
						#map {
					        width: 100%;
					        height: 400px;
					        background-color: grey;
					      }
					</style>
					<script>
					jQuery(function($){
						initMap();
					})
					function initMap() {
						var locs = {lat: '.$entry['17'].', lng: '.$entry['18'].'};
						var map = new google.maps.Map(document.getElementById("map"), {
							zoom: 5,
							center: locs
						});
						var marker = new google.maps.Marker({
							position: locs,
							map: map
						});
					}
					</script>
				   ';

		// $content .= '<br>'.$entry['5.5'].'';
		// $content .= '<br>'.$entry['5.6'].'</p>';

		$content .='<br>';

		$content .= '<pre>';
		// $content .= print_r($entry, true);
		$content .= '</pre>';

		return $content;
	}
	else{
		return '<h3>gak ada</h3>';
	}

}

add_shortcode( 'madewow_practicioner_testimonial', 'madewow_practicioner_testimonial' );
function madewow_practicioner_testimonial( $atts ){
	global $pract_id;
	if ($pract_id) {
		if ( is_user_logged_in() ) {
			add_filter( 'gform_field_value_1', 'custom_populate_userid' );
			
			add_filter( 'gform_field_value_3', 'custom_populate_practicionerid' );
			$content ='';
			$content .=  do_shortcode('[gravityform id="4" title="true" description="true" ajax="true"]');
			return $content;
		}
		else{
			return 'login to leave testimonial';
		}
	}
	
}

add_shortcode( 'madewow_testimonial_edit_display', 'madewow_testimonial_edit_display' );
function madewow_testimonial_edit_display( $atts ){
	$pract_id = $_GET['id'];
	$search_criteria['field_filters'][] = array( 'key' => '3', 'value' => $pract_id );
	$search_criteria['status'] = 'active';
	$entry = GFAPI::get_entries(4,$search_criteria);
	// $entry = $entry[0];

	if ($entry) {
		$content="<pre>";
		$content= print_r($entry,true);
		$content.="</pre>";

		return $content;
	}
	else{
		return '<h3>gak ada</h3>';
	}

}

function custom_populate_practicionerid( $value ) {
    global $pract_id;return $pract_id;
}
function custom_populate_userid( $value ) {
    return get_current_user_id();
}