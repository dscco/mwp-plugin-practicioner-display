<?php 
global $pract_id;
$pract_id = get_current_user_id();




add_shortcode( 'madewow_practicioner_testimonial', 'madewow_practicioner_testimonial' );
function madewow_practicioner_testimonial( $atts ){
	global $pract_id;
	if ($pract_id) {
		if ( is_user_logged_in() ) {
			add_filter( 'gform_field_value_1', 'custom_populate_userid' );
			
			add_filter( 'gform_field_value_3', 'custom_populate_practicionerid' );
			$content ='';
			$content .=  do_shortcode('[gravityform id="4" title="true" description="true" ajax="true"]');
			return $content;
		}
		else{
			return 'login to leave testimonial';
		}
	}
	
}
function custom_populate_practicionerid( $value ) {
    global $pract_id;return $pract_id;
}
// function custom_populate_userid( $value ) {
//     return get_current_user_id();
// }